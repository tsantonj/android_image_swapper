package com.ai08.estusexyswapper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private String DEBUG_TAG = "Gesture";
    private ImageView estuSexyPicturesBox;
    private GestureDetectorCompat gestureDetector;
    private List<Integer> imageList = new ArrayList<>();
    private int image;
    private Animation fadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();

        gestureDetector = new GestureDetectorCompat(this, new GestureListener());
    }

    protected void initComponent() {
        estuSexyPicturesBox = findViewById(R.id.estuSexyPicturesBox);

        imageList.add(R.drawable.ic_taiwan_lantern);
        imageList.add(R.drawable.ic_cambodia);
        imageList.add(R.drawable.ic_singap_night);
        imageList.add(R.drawable.ic_taiwan);

        image = 0;

        setImageUp();

        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
    }


    protected void setImageUp() {
        image += 1;
        if (image >= imageList.size())
            image = 0;
        estuSexyPicturesBox.setImageResource(imageList.get(image));
        //estuSexyPicturesBox.startAnimation(fadeIn);
    }


    protected void setImageDown() {
        image -= 1;
        if (image < 0)
            image = imageList.size() - 1;
        estuSexyPicturesBox.setImageResource(imageList.get(image));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        /*
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                setImage();
                Log.d(DEBUG_TAG,"Action was MOVE");
                break;

            case MotionEvent.ACTION_OUTSIDE:
                Log.d(DEBUG_TAG,"Movement occurred outside bounds " +
                        "of current screen element");
                break;
        }*/
        return super.onTouchEvent(event);
    }


    class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
            if (velocityX > 0 && (-1000 > velocityY || velocityY < 1000 ))
                setImageUp();
            if (velocityX < 0 && (-1000 > velocityY || velocityY < 1000 ))
                setImageDown();

            Log.d(DEBUG_TAG,"onDown: " /* + event1.toString()*/ + velocityY);

            return true;
        }

        @Override
        public void onLongPress(MotionEvent event) {
            Toast.makeText(getApplicationContext(), "image num: " + image, Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onDoubleTap(MotionEvent event) {
            setImageUp();

            return true;
        }
    }
}
